;;; Commentary:
;; Test resources should not be generated for it might render the test suite
;; non-reproducible.  Indeed, the results of Org export depend on the version of
;; Org.
;;
;; We only use this file to bootstrap the test resources.  The resources are
;; then versioned to ensure their consistency.

(require 'ox)

;; Get rid of index.html~ and the like that pop up during generation.
(setq make-backup-files nil)

(defun feed-build-test--export (input output)
  (with-temp-buffer
    (insert-file-contents input)
    (org-export-to-file 'html output)))

(defun feed-builder-test-gen ()
  (let ((org-html-head-include-default-style nil)
        (org-html-head-include-scripts nil))
    (feed-build-test--export "post0.org" "post0.html"))
  (let ((org-html-head-include-default-style nil)
        (org-html-head-include-scripts nil)
        (org-html-doctype "html5"))
    (feed-build-test--export "post0.org" "post0-html5.html"))
  (let ((org-html-head-include-default-style nil)
        (org-html-head-include-scripts nil)
        (org-html-doctype "html5")
        (org-html-html5-fancy t))
    (feed-build-test--export "post0.org" "post0-html5-fancy.html")))
