#!/bin/sh

rm -f site-cert.cache
rm -f site-org.cache
rm -f site-static.cache
rm -f source/articles.org
rm -rf public
mkdir public
cd public
ln -s ./ public
cd ..
emacs --quick --script publish.el --funcall=ambrevar/publish
