#+title: Divan Santana's homepage source

This is the source for my homepage hosted at https://divansantana.com.

The website is generated with

#+BEGIN_SRC sh
emacs --quick --script publish.el --funcall=ambrevar/publish
#+END_SRC

The setup was largely copied from [[https://ambrevar.xyz/index.html][Pierre Neidhartdt great blog site]].
See https://ambrevar.xyz/blog-architecture for a discussion over the
technical details design decisions of his blog.

This is a setup I like the most amoung many great different static
site generator blogs out there.
